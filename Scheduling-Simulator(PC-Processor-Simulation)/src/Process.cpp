#include <iostream>
#include <random>
#include "Process.hpp"
#include "Virtual_Machine.hpp"

Process::Process(){}

Process::Process(int setProcessID, int num_of_IO_Devices, int numOfPages){

	process_ID = setProcessID;
	Process_next_Task = CREATE;

	resourceID = -1;

	approxDuration = 0.0;

	first_IO_Time = 0.0;
	first_IO_Done = false;
	last_CPU_Time = 0.0;

	operationNumber = 0;
	numberOfOperations = getRandNum(controller.PROCESS_OPERATIONS_MIN_COUNT, controller.PROCESS_OPERATIONS_MAX_COUNT);
	if (numberOfOperations % 2 == 0) numberOfOperations += 1; // forces an odd number of operations; AKA, forces operation list to end with a CPU

	if (controller.taskMix > 100) controller.taskMix = 100;
	if (controller.taskMix < 0) controller.taskMix = 0;

	if (controller.taskMix < 50){
		IO_DURATION_MIN_LIMIT = controller.taskMix_min_limit;
		IO_DURATION_MAX_LIMIT = (controller.taskMix_min_limit + (((controller.taskMix * 2) * (controller.taskMix_max_limit - controller.taskMix_min_limit)) / 100));
		CPU_DURATION_MIN_LIMIT = controller.taskMix_min_limit;
		CPU_DURATION_MAX_LIMIT = controller.taskMix_max_limit;
	}
	if (controller.taskMix == 50){
		IO_DURATION_MIN_LIMIT = controller.taskMix_min_limit;
		IO_DURATION_MAX_LIMIT = controller.taskMix_max_limit;
		CPU_DURATION_MIN_LIMIT = controller.taskMix_min_limit;
		CPU_DURATION_MAX_LIMIT = controller.taskMix_max_limit;
	}
	if (controller.taskMix > 50){
		IO_DURATION_MIN_LIMIT = controller.taskMix_min_limit;
		IO_DURATION_MAX_LIMIT = controller.taskMix_max_limit;
		CPU_DURATION_MIN_LIMIT = controller.taskMix_min_limit;
		CPU_DURATION_MAX_LIMIT = (controller.taskMix_min_limit + (((((controller.taskMix - 100) * -1) * 2) * (controller.taskMix_max_limit - controller.taskMix_min_limit)) / 100));
	}


	PROCESS_DURATION_MIN_LIMIT = controller.taskFrequency_min_limit;
	PROCESS_DURATION_MAX_LIMIT = (controller.taskFrequency_min_limit + (((controller.taskFrequency * 2) * (controller.taskFrequency_max_limit - controller.taskFrequency_min_limit)) / 100));
	startTime = getRandNum(PROCESS_DURATION_MIN_LIMIT, PROCESS_DURATION_MAX_LIMIT);
	nextOperationTime = startTime;


	processOperationList.push_back(makeCPUoperation(numOfPages));

	if (numberOfOperations > 1){
		int whichOperation = 1;
		int last = 1;
		for (int i = 0; i < numberOfOperations - 2; i++){

			last = whichOperation;
			if (last == 2) whichOperation = 1;
			else whichOperation = getRandNum(1, 3);
			if (whichOperation == 1) processOperationList.push_back(makeCPUoperation(numOfPages));
			if (whichOperation == 2) {
				processOperationList.push_back(makeIOoperation(num_of_IO_Devices));
			}

		}

		processOperationList.push_back(makeCPUoperation(numOfPages));
	}

	//int whichOperation = 0;  // 0 = CPU; 1 = I/O
	//for (int i = 0; i < numberOfOperations; i++){
	//	switch (whichOperation) { // this forces the operations to alternate between CPU and IO
	//	case 0:
	//		processOperationList.push_back(makeCPUoperation(numOfPages));
	//		whichOperation = 1;
	//		break;
	//	case 1:
	//		processOperationList.push_back(makeIOoperation(num_of_IO_Devices));
	//		whichOperation = 0;
	//		break;
	//	}
	//}

}


Operation Process::makeCPUoperation(int numOfPages){
	return{ Operation::Type::cpu, -1, getRandNum(CPU_DURATION_MIN_LIMIT, CPU_DURATION_MAX_LIMIT), assignPageNum(numOfPages), Operation::Status::wait };
}
Operation Process::makeIOoperation(int num_of_IO_Devices){
	return{ Operation::Type::io, getRandNum(0, num_of_IO_Devices), getRandNum(IO_DURATION_MIN_LIMIT, IO_DURATION_MAX_LIMIT), Operation::Status::wait };
}

void Process::setApproxDurration(){
	int totalCount = 0;
	approxDuration = 0.0;
	if (Process_next_Task == Process_Task::CPU){
		for (int i = 0; i < processOperationList.size(); i++){
			if (processOperationList[i].operationType == Operation::Type::cpu && processOperationList[i].status == Operation::Status::done){
				approxDuration += processOperationList[i].duration;
				totalCount++;
			}
		}
	}
	if (Process_next_Task == Process_Task::IO){
		for (int i = 0; i < processOperationList.size(); i++){
			if (processOperationList[i].operationType == Operation::Type::io && processOperationList[i].status == Operation::Status::done){
				approxDuration += processOperationList[i].duration;
				totalCount++;
			}
		}
	}
	if (totalCount > 0) approxDuration /= totalCount;
	else totalCount = 0;
}

template< class T >
T Process::getRandNum(T lowerBound, T upperBound){
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_real_distribution<> urd(lowerBound, upperBound);
	return urd(mt);
}

int Process::assignPageNum(int numOfPages){
	std::vector<int> temp;
	for (int i = 1; i <= numOfPages; i++){
		temp.push_back(getRandNum(controller.pageDistribution_MIN_LIMIT, controller.pageDistribution_MAX_LIMIT));
	}
	int tempNum = getRandNum(1, numOfPages);
	return temp[tempNum - 1];
}