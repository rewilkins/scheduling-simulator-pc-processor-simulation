#include <iostream>
#include <string>
#include <algorithm>
#include "Statistics.hpp"
#include "Virtual_Machine.hpp"

int main(){

	std::string selection1;
	std::string selection2;

	std::cout << "------------------------------------------------------" << std::endl;
	std::cout << "\tName\t\tDiscription" << std::endl;
	std::cout << "\t----\t\t-----------" << std::endl << std::endl;
	std::cout << "\t\"FIFO\" \t\tfor First In First Out" << std::endl;
	std::cout << "\t\"PQ\" \t\tfor Shortest Duration First" << std::endl;
	std::cout << "\t\"ApproxPQ\" \tfor Estimated Shortest Duration First" << std::endl;
	std::cout << "\t\"RR\" \t\tfor Round Robin" << std::endl;
	std::cout << "\t\"ALL\" \t\tfor running every algorithm with the same data" << std::endl;
	std::cout << "------------------------------------------------------" << std::endl;
	std::cout << "Type a scheduling algorithm Name then hit \"Enter\": ";
	std::cin >> selection1;
	system("cls");

	std::transform(selection1.begin(), selection1.end(), selection1.begin(), ::tolower);

	std::cout << "------------------------------------------------------" << std::endl;
	std::cout << "\tName\t\tDiscription" << std::endl;
	std::cout << "\t----\t\t-----------" << std::endl << std::endl;
	std::cout << "\t\"FIFO\" \t\tfor First In First Out" << std::endl;
	std::cout << "\t\"MOST\" \t\tfor most recently used First" << std::endl;
	std::cout << "\t\"LEAST\" \tfor least recently used First" << std::endl;
	std::cout << "\t\"SC\" \t\tfor second chance" << std::endl;
	std::cout << "\t\"ALL\" \t\tfor running every algorithm with the same data" << std::endl;
	std::cout << "------------------------------------------------------" << std::endl;
	std::cout << "Type a memory algorithm Name then hit \"Enter\": ";
	std::cin >> selection2;
	system("cls");

	std::transform(selection2.begin(), selection2.end(), selection2.begin(), ::tolower);
	
	Virtual_Machine vm(selection1, selection2);


	int returnVal;
	if (selection1 != "all") {
		if (selection2 != "all"){ // not all & not all
			returnVal = vm.start();

			std::cout << "-------------------------" << std::endl;
			if (returnVal == 0) std::cout << "V.M. finished successfully." << std::endl;
			if (returnVal == 1) std::cout << "V.M. reached maximun allowed runtime.  V.M. terminated early." << std::endl;
			if (returnVal == 2) {
				std::cout << "Scheduling algorithm type \"" << selection1 << "\" does not exist." << std::endl;
				std::cout << "OR..." << std::endl;
				std::cout << "Memory algorithm type \"" << selection2 << "\" does not exist." << std::endl;
				std::cout << "V.M. failed to start." << std::endl;
			}
			std::cout << "-------------------------------------------------" << std::endl;
		}
		else{ // not all & all
			int numOfAlgorithms = 4;
			for (int i = 1; i <= numOfAlgorithms; i++){

				if (i == 1) vm.setMemoryAlgorithmType(MemoryAlgorithmType::MEM_FIFO);
				if (i == 2) vm.setMemoryAlgorithmType(MemoryAlgorithmType::MEM_MOST);
				if (i == 3) vm.setMemoryAlgorithmType(MemoryAlgorithmType::MEM_LEAST);
				if (i == 4) vm.setMemoryAlgorithmType(MemoryAlgorithmType::MEM_SC);

				returnVal = vm.start();

				std::cout << "-------------------------" << std::endl;
				if (returnVal == 0) std::cout << "V.M. finished successfully." << std::endl;
				if (returnVal == 1) std::cout << "V.M. reached maximun allowed runtime.  V.M. terminated early." << std::endl;
				if (returnVal == 2) {
					std::cout << "Scheduling algorithm type \"" << selection1 << "\" does not exist." << std::endl;
					std::cout << "OR..." << std::endl;
					std::cout << "Memory algorithm type \"" << selection2 << "\" does not exist." << std::endl;
					std::cout << "V.M. failed to start." << std::endl;
				}
				std::cout << "-------------------------------------------------" << std::endl;

				if (i < numOfAlgorithms) vm.resetVM();
			}
		}
	}
	else{
		if (selection2 != "all"){ // all & not all
			int numOfAlgorithms = 4;
			for (int i = 1; i <= numOfAlgorithms; i++){

				if (i == 1) vm.setSchedulingAlgorithmType(SchedulingAlgorithmType::FIFO);
				if (i == 2) vm.setSchedulingAlgorithmType(SchedulingAlgorithmType::PQ);
				if (i == 3) vm.setSchedulingAlgorithmType(SchedulingAlgorithmType::ApproxPQ);
				if (i == 4) vm.setSchedulingAlgorithmType(SchedulingAlgorithmType::RR);

				returnVal = vm.start();

				std::cout << "-------------------------" << std::endl;
				if (returnVal == 0) std::cout << "V.M. finished successfully." << std::endl;
				if (returnVal == 1) std::cout << "V.M. reached maximun allowed runtime.  V.M. terminated early." << std::endl;
				if (returnVal == 2) {
					std::cout << "Scheduling algorithm type \"" << selection1 << "\" does not exist." << std::endl;
					std::cout << "OR..." << std::endl;
					std::cout << "Memory algorithm type \"" << selection2 << "\" does not exist." << std::endl;
					std::cout << "V.M. failed to start." << std::endl;
				}
				std::cout << "-------------------------------------------------" << std::endl;

				if (i < numOfAlgorithms) vm.resetVM();
			}
		}
		else{ // all & all
			int numOfAlgorithms = 4;
			for (int i = 1; i <= numOfAlgorithms; i++){

				if (i == 1) vm.setSchedulingAlgorithmType(SchedulingAlgorithmType::FIFO);
				if (i == 2) vm.setSchedulingAlgorithmType(SchedulingAlgorithmType::PQ);
				if (i == 3) vm.setSchedulingAlgorithmType(SchedulingAlgorithmType::ApproxPQ);
				if (i == 4) vm.setSchedulingAlgorithmType(SchedulingAlgorithmType::RR);

				for (int j = 1; j <= numOfAlgorithms; j++){

					if (j == 1) vm.setMemoryAlgorithmType(MemoryAlgorithmType::MEM_FIFO);
					if (j == 2) vm.setMemoryAlgorithmType(MemoryAlgorithmType::MEM_MOST);
					if (j == 3) vm.setMemoryAlgorithmType(MemoryAlgorithmType::MEM_LEAST);
					if (j == 4) vm.setMemoryAlgorithmType(MemoryAlgorithmType::MEM_SC);

					returnVal = vm.start();

					std::cout << "-------------------------" << std::endl;
					if (returnVal == 0) std::cout << "V.M. finished successfully." << std::endl;
					if (returnVal == 1) std::cout << "V.M. reached maximun allowed runtime.  V.M. terminated early." << std::endl;
					if (returnVal == 2) {
						std::cout << "Scheduling algorithm type \"" << selection1 << "\" does not exist." << std::endl;
						std::cout << "OR..." << std::endl;
						std::cout << "Memory algorithm type \"" << selection2 << "\" does not exist." << std::endl;
						std::cout << "V.M. failed to start." << std::endl;
					}
					std::cout << "-------------------------------------------------" << std::endl;

					if (j < numOfAlgorithms) vm.resetVM();
				}
				if (i < numOfAlgorithms) vm.resetVM();
			}
		}
	}

	system("pause");
	return 0;

}
