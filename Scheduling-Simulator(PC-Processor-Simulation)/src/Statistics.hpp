#ifndef STATISTICS_HPP
#define STATISTICS_HPP

#include <iostream>
#include <vector>
#include <iomanip>
#include "Process.hpp"

class Statistics {
public:

	std::vector<double> v_ResponseTime;		// Time from process creation to first I/O completion
	double averageResponseTime = 0.0;
	std::vector<double> v_Latency;			// Time from process creation to final CPU completion
	double averageLatency = 0.0;
	double Throughput = 0.0;						// Number of completed processes per unit time
	double Efficiency = 0.0;						// Percentage of time spent with CPUs occupied with useful work(context switching does not count)

	void resetStats(){
		v_ResponseTime.clear();
		averageResponseTime = 0.0;
		v_Latency.clear();
		averageLatency = 0.0;
		Throughput = 0.0;
		Efficiency = 0.0;
	}

	void storeResponseTime(Process process){
		v_ResponseTime.push_back(process.first_IO_Time - process.startTime);
	}

	void storeLatency(Process process){
		v_Latency.push_back(process.last_CPU_Time - process.startTime);
	}

	void storeThroughput(int numOfTasks){
		makeAverageLatency();
		if (averageLatency > 0) Throughput = numOfTasks / averageLatency;
		else Throughput = -1; // incomplete
	}

	void storeEfficiency(double utilization, double machineTime, int totalCpuCount){
		Efficiency = ((utilization / machineTime / totalCpuCount) * 100);
	}

	void makeAverageLatency(){
		for (int i = 0; i < v_Latency.size(); i++){
			averageLatency += v_Latency[i];
		}
		if (v_Latency.size() != 0) averageLatency /= v_Latency.size();
		else averageLatency = -1; // incomplete
	}

	void makeAverageResponseTime(){
		for (int i = 0; i < v_ResponseTime.size(); i++){
			averageResponseTime += v_ResponseTime[i];
		}
		if (v_ResponseTime.size() != 0) averageResponseTime /= v_ResponseTime.size();
		else averageResponseTime = -1; // incomplete
	}

	void printStats(){

		std::cout << std::fixed;
		std::cout << "-------------------------" << std::endl;
		if (averageResponseTime >= 0) std::cout << "Response Time: " << averageResponseTime << std::endl;
		if (averageResponseTime == -1) std::cout << "Response Time: " << "Incomplete" << std::endl;

		if (averageLatency >= 0) std::cout << "Latency: " << averageLatency << std::endl;
		if (averageLatency == -1) std::cout << "Latency: " << "Incomplete" << std::endl;

		if (Throughput >= 0) std::cout << "Throughput: " << Throughput << std::endl;
		if (Throughput == -1) std::cout << "Throughput: " << "Incomplete" << std::endl;

		std::cout << "Efficiency: " << Efficiency << "%" << std::endl;

	}

	Statistics(){}

};

#endif
