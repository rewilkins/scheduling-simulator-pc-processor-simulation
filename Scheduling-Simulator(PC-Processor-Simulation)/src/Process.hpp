#ifndef PROCESS_HPP
#define PROCESS_HPP

#include <vector>
#include <memory>
#include "Controller.hpp"

enum Process_Task{ CREATE, CPU, IO, STOP, END };

struct Operation{
	enum Type{ cpu, io };
	enum Status{ wait, proc, intr, done };
	Type operationType;
	int ioType;
	double duration;
	int pageNum;
	Status status;
};

class Process{

public:

	Process();
	Process(int setProcessID, int num_of_IO_Devices, int pageDistribution);

	struct compareProcesses_OpTime{
		bool operator()(const Process& lhs, const Process& rhs) const
		{
			return lhs.nextOperationTime > rhs.nextOperationTime;
		}
	};

	struct compareProcesses_ApproxTime{
		bool operator()(const Process& lhs, const Process& rhs) const
		{
			return lhs.approxDuration > rhs.approxDuration;
		}
	};

	int process_ID;
	Process_Task Process_next_Task;

	int operationNumber;
	int numberOfOperations;

	int resourceID;

	double startTime;
	double nextOperationTime;

	double approxDuration;

	double first_IO_Time;
	bool first_IO_Done;

	double last_CPU_Time;

	double CPU_DURATION_MIN_LIMIT;
	double CPU_DURATION_MAX_LIMIT;
	double IO_DURATION_MIN_LIMIT;
	double IO_DURATION_MAX_LIMIT;

	double PROCESS_DURATION_MIN_LIMIT;
	double PROCESS_DURATION_MAX_LIMIT;

	std::vector <Operation> processOperationList;

	Operation makeCPUoperation(int pageDistribution);
	Operation makeIOoperation(int num_of_IO_Devices);

	void setApproxDurration();

	template < class T >
	T getRandNum(T lowerBound, T upperBound);

	int assignPageNum(int numOfPages);

	Controller controller;

};


#endif



