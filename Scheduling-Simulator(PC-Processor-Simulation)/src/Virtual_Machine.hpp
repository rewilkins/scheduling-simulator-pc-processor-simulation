#ifndef VM_FIFO_HPP
#define VM_FIFO_HPP

#include <vector>
#include <queue>
#include <memory>
#include "Process.hpp"
#include "Controller.hpp"
#include "Statistics.hpp"

enum SchedulingAlgorithmType { ERROR, FIFO, PQ, ApproxPQ, RR, ALL };
enum MemoryAlgorithmType { MEM_ERROR, MEM_FIFO, MEM_MOST, MEM_LEAST, MEM_SC, MEM_ALL };

class Virtual_Machine{

public:

	void resetVM();

	void setSchedulingAlgorithmType(SchedulingAlgorithmType input);
	void setMemoryAlgorithmType(MemoryAlgorithmType input);

	Virtual_Machine(std::string selection1, std::string selection2);
	void print_System_Info();
	void print_Cache_vec();
	void print_Process_Queue();
	void print_CPU_Queue();
	void print_IO_Queues();
	double getMachinesTime();
	double getUtilization();

	int start();

private:

	struct MemoryItem
	{
		int pageNum = -1;
		double status = -1.0; // used to notify of things...  not very specific, I know...
		int secondChanceNum = 0.0;
	};
	std::vector<MemoryItem> cacheMemory;


	bool loadSuccessful1;
	bool loadSuccessful2;

	Process process; // temp holding place for when handling queue events

	SchedulingAlgorithmType schedulingAlgorithmType;
	MemoryAlgorithmType memoryAlgorithmType;

	double machinesTime;

	std::vector<bool> CPUs;
	bool CPUavailable;
	std::vector<double> RR_Inter_Durations;
	bool interupted;
	int inter_ResourceID;

	int available_CPUs;
	int totalCpuCount;
	int num_of_IO_Devices;
	int numOfPages;
	int cacheMemorySize;

	double utilization;

	int numOfTasks;

	Statistics statistics;

	std::vector<bool> IO_Device_Busy;

	std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime> process_queue;
	std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime> process_queue_StartingState;  // used for reseting the VM.
	std::queue<Process> cpu_queue_FIFO;
	std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime> cpu_queue_PQ;
	std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_ApproxTime> cpu_queue_ApproxPQ;
	std::queue<Process> cpu_queue_RR;

	std::vector<std::queue<Process>> io_queues_FIFO;
	std::vector<std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime>> io_queues_PQ;
	std::vector<std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_ApproxTime>> io_queues_ApproxPQ;
	std::vector<std::queue<Process>> io_queues_RR;

	void handle_Proccess_Queue();
	void handle_CPU_Queue();
	bool handle_Cache_Vec(int inputPage);
	void handle_Cache_Vec_SC(int inputPage, bool done);
	void handle_IO_Queue();

	template < class T >
	T getRandNum(T lowerBound, T upperBound);

	Controller controller;

};
#endif



