#include <iostream>
#include <iomanip>
#include <algorithm>
#include "Virtual_Machine.hpp"
#include "Process.hpp"

Virtual_Machine::Virtual_Machine(std::string selection1, std::string selection2){

	// load algorithm type
	loadSuccessful1 = false;
	loadSuccessful2 = false;
	if (selection1 == "fifo"){
		schedulingAlgorithmType = SchedulingAlgorithmType::FIFO;
		loadSuccessful1 = true;
	}
	if (selection1 == "pq"){
		schedulingAlgorithmType = SchedulingAlgorithmType::PQ;
		loadSuccessful1 = true;
	}
	if (selection1 == "approxpq"){
		schedulingAlgorithmType = SchedulingAlgorithmType::ApproxPQ;
		loadSuccessful1 = true;
	}
	if (selection1 == "rr"){
		schedulingAlgorithmType = SchedulingAlgorithmType::RR;
		loadSuccessful1 = true;
	}
	if (selection1 == "all"){
		schedulingAlgorithmType = SchedulingAlgorithmType::ALL;
		loadSuccessful1 = true;
	}


	if (selection2 == "fifo"){
		memoryAlgorithmType = MemoryAlgorithmType::MEM_FIFO;
		loadSuccessful2 = true;
	}
	if (selection2 == "most"){
		memoryAlgorithmType = MemoryAlgorithmType::MEM_MOST;
		loadSuccessful2 = true;
	}
	if (selection2 == "least"){
		memoryAlgorithmType = MemoryAlgorithmType::MEM_LEAST;
		loadSuccessful2 = true;
	}
	if (selection2 == "sc"){
		memoryAlgorithmType = MemoryAlgorithmType::MEM_SC;
		loadSuccessful2 = true;
	}
	if (selection2 == "all"){
		memoryAlgorithmType = MemoryAlgorithmType::MEM_ALL;
		loadSuccessful2 = true;
	}

	// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	// end load algorithm type

	if (loadSuccessful1 == true && loadSuccessful2 == true) {

		machinesTime = 0.0;
		utilization = 0.0;

		available_CPUs = getRandNum(controller.CPUs_MIN_LIMIT, controller.CPUs_MAX_LIMIT);
		totalCpuCount = available_CPUs;
		for (int i = 0; i < totalCpuCount; i++){
			CPUs.push_back(true);
			RR_Inter_Durations.push_back(0.0);
		}

		interupted = false;

		num_of_IO_Devices = getRandNum(controller.IO_DEVICE_MIN_LIMIT, controller.IO_DEVICE_MAX_LIMIT);

		numOfTasks = getRandNum(controller.PROCESS_MIN_COUNT, controller.PROCESS_MAX_COUNT);

		for (int i = 0; i < num_of_IO_Devices; i++){
			IO_Device_Busy.push_back(false);

			if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO || schedulingAlgorithmType == SchedulingAlgorithmType::ALL) io_queues_FIFO.push_back(std::queue<Process>());
			if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ || schedulingAlgorithmType == SchedulingAlgorithmType::ALL) io_queues_PQ.push_back(std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime>());
			if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ || schedulingAlgorithmType == SchedulingAlgorithmType::ALL) io_queues_ApproxPQ.push_back(std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_ApproxTime>());
			if (schedulingAlgorithmType == SchedulingAlgorithmType::RR || schedulingAlgorithmType == SchedulingAlgorithmType::ALL) io_queues_RR.push_back(std::queue<Process>());

			// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

		}

		numOfPages = getRandNum(controller.pageDistribution_MIN_LIMIT, controller.pageDistribution_MAX_LIMIT);
		for (int i = 1; i <= numOfTasks; i++){
			process_queue.push(Process(i, num_of_IO_Devices, numOfPages));
		}

		cacheMemorySize = getRandNum(controller.cacheMemorySize_MIN_LIMIT, controller.cacheMemorySize_MAX_LIMIT);
		for (int i = 1; i <= cacheMemorySize; i++){
			cacheMemory.push_back(MemoryItem());
		}

		process_queue_StartingState = process_queue;
	}

}

void Virtual_Machine::resetVM(){
	machinesTime = 0.0;
	utilization = 0.0;
	available_CPUs = totalCpuCount;
	for (int i = 0; i < totalCpuCount; i++){
		CPUs[i] == true;
		RR_Inter_Durations[i] == 0.0;
	}
	interupted = false;
	while (!cpu_queue_FIFO.empty())cpu_queue_FIFO.pop();
	while (!cpu_queue_PQ.empty())cpu_queue_PQ.pop();
	while (!cpu_queue_ApproxPQ.empty())cpu_queue_ApproxPQ.pop();
	while (!cpu_queue_RR.empty())cpu_queue_RR.pop();

	// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	for (int i = 0; i < num_of_IO_Devices; i++){
		IO_Device_Busy[i] == false;
		if (schedulingAlgorithmType == FIFO) while(!io_queues_FIFO[i].empty()) io_queues_FIFO[i].pop();
		if (schedulingAlgorithmType == PQ) while (!io_queues_PQ[i].empty()) io_queues_PQ[i].pop();
		if (schedulingAlgorithmType == ApproxPQ) while (!io_queues_ApproxPQ[i].empty()) io_queues_ApproxPQ[i].pop();
		if (schedulingAlgorithmType == RR) while (!io_queues_RR[i].empty()) io_queues_RR[i].pop();

		// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	}
	cacheMemory.clear();
	for (int i = 1; i <= cacheMemorySize; i++){
		cacheMemory.push_back(MemoryItem());
	}
	process_queue = process_queue_StartingState;
	statistics.resetStats();
}

void Virtual_Machine::setSchedulingAlgorithmType(SchedulingAlgorithmType input){
	schedulingAlgorithmType = input;
}

void Virtual_Machine::setMemoryAlgorithmType(MemoryAlgorithmType input){
	memoryAlgorithmType = input;
}

void Virtual_Machine::print_System_Info(){

	std::cout << std::fixed;

	std::cout << "------------------------" << std::endl;
	std::cout << "Scheduling Algorithm Type: ";
	if (schedulingAlgorithmType == SchedulingAlgorithmType::ERROR) std::cout << "ERROR" << std::endl;
	if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) std::cout << "FIFO" << std::endl;
	if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) std::cout << "PQ" << std::endl;
	if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) std::cout << "ApproxPQ" << std::endl;
	if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) std::cout << "RR" << std::endl;

	// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	std::cout << "Memory Algorithm Type: ";
	if (memoryAlgorithmType == MemoryAlgorithmType::MEM_ERROR) std::cout << "ERROR" << std::endl;
	if (memoryAlgorithmType == MemoryAlgorithmType::MEM_FIFO) std::cout << "FIFO" << std::endl;
	if (memoryAlgorithmType == MemoryAlgorithmType::MEM_MOST) std::cout << "MOST" << std::endl;
	if (memoryAlgorithmType == MemoryAlgorithmType::MEM_LEAST) std::cout << "LEAST" << std::endl;
	if (memoryAlgorithmType == MemoryAlgorithmType::MEM_SC) std::cout << "SC" << std::endl;

	// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	std::cout << "Machine's Time: " << machinesTime << std::endl;
	std::cout << "Machine's Max Runtime: " << controller.max_runtime << std::endl;
	std::cout << "Effective CPU Time: " << utilization << std::endl << std::endl;
	std::cout << "Cache Size: " << cacheMemorySize << std::endl;
	std::cout << "Number of Pages: " << numOfPages << std::endl;
	std::cout << "Num of CPUs: " << totalCpuCount << std::endl;
	std::cout << "Num of IO devices: " << num_of_IO_Devices << std::endl << std::endl;
	std::cout << "Num of Available CPUs: " << available_CPUs << std::endl;
	std::cout << "------------------------" << std::endl;

}

void Virtual_Machine::print_Cache_vec(){

	std::cout << std::endl;
	std::cout << "------------------------------------------------" << std::endl;
	std::cout << "Cache Vector" << std::endl;
	std::cout << "-------------" << std::endl;
	std::cout << std::endl;

	for (int i = 0; i < cacheMemory.size(); i++){
		std::cout << std::setprecision(1);
		if (cacheMemory[i].pageNum == -1) std::cout << "[EMPTY]";
		else std::cout << "[" << cacheMemory[i].pageNum << "," << cacheMemory[i].status << "," << cacheMemory[i].secondChanceNum << "]";
		if (i != cacheMemory.size() - 1) std::cout << " ";
		std::cout << std::setprecision(7);
	}

	std::cout << std::endl << std::endl;

}

void Virtual_Machine::print_Process_Queue(){

	std::cout << std::fixed;

	std::cout << std::endl;
	std::cout << "------------------------------------------------" << std::endl;

	std::vector<Operation> v_process_operations;
	std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime> process_queue_copy = process_queue;


	std::cout << "Process Queue" << std::endl;
	std::cout << "-------------" << std::endl;
	std::cout << std::endl;


	if (process_queue_copy.empty()){
		std::cout << "\t*Empty Queue*" << std::endl;
	}

	while (!process_queue_copy.empty()){
		if (process_queue_copy.top().Process_next_Task == CREATE) std::cout << "CREATE";
		if (process_queue_copy.top().Process_next_Task == CPU) std::cout << "CPU";
		if (process_queue_copy.top().Process_next_Task == IO) std::cout << "IO";
		if (process_queue_copy.top().Process_next_Task == STOP) std::cout << "STOP";
		if (process_queue_copy.top().Process_next_Task == END) std::cout << "END";
		std::cout << " - \"Process " << process_queue_copy.top().process_ID << "\"";
		std::cout << " - Next Opr. Time: " << process_queue_copy.top().nextOperationTime;

		std::cout << std::endl;

		bool found = false;
		v_process_operations = process_queue_copy.top().processOperationList;
		for (int j = 0; j < v_process_operations.size(); j++){

			//process.processOperationList[process.operationNumber].status = Operation::Status::intr;

			std::cout << "\t";
			if (v_process_operations[j].operationType == v_process_operations[j].cpu){
				std::cout << "CPU - Pg: " << v_process_operations[j].pageNum << "\t";
			}
			else {
				std::cout << "IO" << v_process_operations[j].ioType << " - Pg: NULL\t";
			}
			std::cout << " - " << v_process_operations[j].duration;
			std::cout << " - ";

			if (v_process_operations[j].status == Operation::Status::wait){
				std::cout << "Wait";
				if (found == false){
					found = true;
					if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) std::cout << " <-- Approx Duration: " << process_queue_copy.top().approxDuration;
					else if (schedulingAlgorithmType == SchedulingAlgorithmType::RR && process_queue_copy.top().Process_next_Task == CPU) {
						if (process_queue_copy.top().resourceID != -1){
							std::cout << " <-- Cutoff Time: " << RR_Inter_Durations[process_queue_copy.top().resourceID];
						}
						else{
							std::cout << " <--";
						}
					}
					else std::cout << " <--";
				}
			}

			if (v_process_operations[j].status == Operation::Status::proc){
				std::cout << "Proc";
				if (found == false){
					found = true;
					if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) std::cout << " <-- Approx Duration: " << process_queue_copy.top().approxDuration;
					else if (schedulingAlgorithmType == SchedulingAlgorithmType::RR && process_queue_copy.top().Process_next_Task == CPU) {
						if (process_queue_copy.top().resourceID != -1){
							std::cout << " <-- Cutoff Time: " << RR_Inter_Durations[process_queue_copy.top().resourceID];
						}
						else{
							std::cout << " <--";
						}
					}
					else std::cout << " <--";
				}
			}

			if (v_process_operations[j].status == Operation::Status::intr){
				std::cout << "Intr";
				if (found == false){
					found = true;
					if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) std::cout << " <-- Approx Duration: " << process_queue_copy.top().approxDuration;
					else if (schedulingAlgorithmType == SchedulingAlgorithmType::RR && process_queue_copy.top().Process_next_Task == CPU) {
						if (process_queue_copy.top().resourceID != -1){
							std::cout << " <-- Cutoff Time: " << RR_Inter_Durations[process_queue_copy.top().resourceID];
						}
						else{
							std::cout << " <--";
						}
					}
					else std::cout << " <--";
				}
			}

			if (v_process_operations[j].status == Operation::Status::done){
				std::cout << "Done";
			}

			std::cout << std::endl;

		}

		process_queue_copy.pop();

	}

}

void Virtual_Machine::print_CPU_Queue(){

	std::cout << std::fixed;

	bool found = false;

	std::cout << std::endl;
	std::cout << "------------------------------------------------" << std::endl;
	std::cout << "CPU Queue" << std::endl;
	std::cout << "-------------" << std::endl;
	std::cout << std::endl;

	std::vector<Operation> v_CPU_operations;

	if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO){

		std::queue<Process> CPU_queue_copy = cpu_queue_FIFO;

		if (CPU_queue_copy.empty()){
			std::cout << "\t*Empty Queue*" << std::endl;
		}

		while (!CPU_queue_copy.empty()){
			if (CPU_queue_copy.front().Process_next_Task == CREATE) std::cout << "CREATE";
			if (CPU_queue_copy.front().Process_next_Task == CPU) std::cout << "CPU";
			if (CPU_queue_copy.front().Process_next_Task == IO) std::cout << "IO";
			if (CPU_queue_copy.front().Process_next_Task == STOP) std::cout << "STOP";
			if (CPU_queue_copy.front().Process_next_Task == END) std::cout << "END";
			std::cout << " - \"Process " << CPU_queue_copy.front().process_ID << "\"";
			std::cout << " - Next Opr. Time: " << CPU_queue_copy.front().nextOperationTime;

			std::cout << std::endl;

			found = false;
			v_CPU_operations = CPU_queue_copy.front().processOperationList;
			for (int j = 0; j < v_CPU_operations.size(); j++){

				std::cout << "\t";
				if (v_CPU_operations[j].operationType == v_CPU_operations[j].cpu){
					std::cout << "CPU - Pg: " << v_CPU_operations[j].pageNum << "\t";
				}
				else {
					std::cout << "IO" << v_CPU_operations[j].ioType << " - Pg: NULL\t";
				}
				std::cout << " - " << v_CPU_operations[j].duration;
				std::cout << " - ";

				if (v_CPU_operations[j].status == Operation::Status::wait){
					std::cout << "Wait";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.front().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::proc){
					std::cout << "Proc";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.front().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::intr){
					std::cout << "Intr";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.front().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::done){
					std::cout << "Done";
				}

				std::cout << std::endl;
			}

			CPU_queue_copy.pop();

		}
	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ){

		std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime> CPU_queue_copy = cpu_queue_PQ;

		if (CPU_queue_copy.empty()){
			std::cout << "\t*Empty Queue*" << std::endl;
		}

		while (!CPU_queue_copy.empty()){
			if (CPU_queue_copy.top().Process_next_Task == CREATE) std::cout << "CREATE";
			if (CPU_queue_copy.top().Process_next_Task == CPU) std::cout << "CPU";
			if (CPU_queue_copy.top().Process_next_Task == IO) std::cout << "IO";
			if (CPU_queue_copy.top().Process_next_Task == STOP) std::cout << "STOP";
			if (CPU_queue_copy.top().Process_next_Task == END) std::cout << "END";
			std::cout << " - \"Process " << CPU_queue_copy.top().process_ID << "\"";
			std::cout << " - Next Opr. Time: " << CPU_queue_copy.top().nextOperationTime;

			std::cout << std::endl;

			found = false;
			v_CPU_operations = CPU_queue_copy.top().processOperationList;
			for (int j = 0; j < v_CPU_operations.size(); j++){

				std::cout << "\t";
				if (v_CPU_operations[j].operationType == v_CPU_operations[j].cpu){
					std::cout << "CPU - Pg: " << v_CPU_operations[j].pageNum << "\t";
				}
				else {
					std::cout << "IO" << v_CPU_operations[j].ioType << " - Pg: NULL\t";
				}
				std::cout << " - " << v_CPU_operations[j].duration;
				std::cout << " - ";

				if (v_CPU_operations[j].status == Operation::Status::wait){
					std::cout << "Wait";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.top().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::proc){
					std::cout << "Proc";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.top().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::intr){
					std::cout << "Intr";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.top().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::done){
					std::cout << "Done";
				}

				std::cout << std::endl;

			}

			CPU_queue_copy.pop();

		}

	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ){

		std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_ApproxTime> CPU_queue_copy = cpu_queue_ApproxPQ;

		if (CPU_queue_copy.empty()){
			std::cout << "\t*Empty Queue*" << std::endl;
		}

		while (!CPU_queue_copy.empty()){
			if (CPU_queue_copy.top().Process_next_Task == CREATE) std::cout << "CREATE";
			if (CPU_queue_copy.top().Process_next_Task == CPU) std::cout << "CPU";
			if (CPU_queue_copy.top().Process_next_Task == IO) std::cout << "IO";
			if (CPU_queue_copy.top().Process_next_Task == STOP) std::cout << "STOP";
			if (CPU_queue_copy.top().Process_next_Task == END) std::cout << "END";
			std::cout << " - \"Process " << CPU_queue_copy.top().process_ID << "\"";
			std::cout << " - Next Opr. Time: " << CPU_queue_copy.top().nextOperationTime;

			std::cout << std::endl;

			found = false;
			v_CPU_operations = CPU_queue_copy.top().processOperationList;
			for (int j = 0; j < v_CPU_operations.size(); j++){

				std::cout << "\t";
				if (v_CPU_operations[j].operationType == v_CPU_operations[j].cpu){
					std::cout << "CPU - Pg: " << v_CPU_operations[j].pageNum << "\t";
				}
				else {
					std::cout << "IO" << v_CPU_operations[j].ioType << " - Pg: NULL\t";
				}
				std::cout << " - " << v_CPU_operations[j].duration;
				std::cout << " - ";

				if (v_CPU_operations[j].status == Operation::Status::wait){
					std::cout << "Wait";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.top().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::proc){
					std::cout << "Proc";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.top().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::intr){
					std::cout << "Intr";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.top().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::done){
					std::cout << "Done";
				}

				std::cout << std::endl;

			}

			CPU_queue_copy.pop();

		}

	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::RR){

		std::queue<Process> CPU_queue_copy = cpu_queue_RR;

		if (CPU_queue_copy.empty()){
			std::cout << "\t*Empty Queue*" << std::endl;
		}

		while (!CPU_queue_copy.empty()){
			if (CPU_queue_copy.front().Process_next_Task == CREATE) std::cout << "CREATE";
			if (CPU_queue_copy.front().Process_next_Task == CPU) std::cout << "CPU";
			if (CPU_queue_copy.front().Process_next_Task == IO) std::cout << "IO";
			if (CPU_queue_copy.front().Process_next_Task == STOP) std::cout << "STOP";
			if (CPU_queue_copy.front().Process_next_Task == END) std::cout << "END";
			std::cout << " - \"Process " << CPU_queue_copy.front().process_ID << "\"";
			std::cout << " - Next Opr. Time: " << CPU_queue_copy.front().nextOperationTime;

			std::cout << std::endl;

			found = false;
			v_CPU_operations = CPU_queue_copy.front().processOperationList;
			for (int j = 0; j < v_CPU_operations.size(); j++){

				std::cout << "\t";
				if (v_CPU_operations[j].operationType == v_CPU_operations[j].cpu){
					std::cout << "CPU - Pg: " << v_CPU_operations[j].pageNum << "\t";
				}
				else {
					std::cout << "IO" << v_CPU_operations[j].ioType << " - Pg: NULL\t";
				}
				std::cout << " - " << v_CPU_operations[j].duration;
				std::cout << " - ";

				if (v_CPU_operations[j].status == Operation::Status::wait){
					std::cout << "Wait";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.front().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::proc){
					std::cout << "Proc";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.front().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::intr){
					std::cout << "Intr";
					if (found == false){
						found = true;
						if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
						else std::cout << " <-- Approx Duration: " << CPU_queue_copy.front().approxDuration;
					}
				}

				if (v_CPU_operations[j].status == Operation::Status::done){
					std::cout << "Done";
				}

				std::cout << std::endl;

			}

			CPU_queue_copy.pop();

		}

	}



	// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //


}

void Virtual_Machine::print_IO_Queues(){

	std::cout << std::fixed;

	bool found = false;

	std::vector<Operation> v_IO_operations;

	for (int i = 0; i < num_of_IO_Devices; i++){

		std::cout << std::endl;
		std::cout << "------------------------------------------------" << std::endl;
		std::cout << "IO Queue " << i << std::endl;
		std::cout << "-------------" << std::endl;
		std::cout << std::endl;

		if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO){

			std::vector<std::queue<Process>> IO_queue_copy = io_queues_FIFO;

			if (IO_queue_copy[i].empty()){
				std::cout << "\t*Empty Queue*" << std::endl << std::endl;
			}

			while (!IO_queue_copy[i].empty()){
				if (IO_queue_copy[i].front().Process_next_Task == CREATE) std::cout << "CREATE";
				if (IO_queue_copy[i].front().Process_next_Task == CPU) std::cout << "CPU";
				if (IO_queue_copy[i].front().Process_next_Task == IO) std::cout << "IO";
				if (IO_queue_copy[i].front().Process_next_Task == STOP) std::cout << "STOP";
				if (IO_queue_copy[i].front().Process_next_Task == END) std::cout << "END";
				std::cout << " - \"Process " << IO_queue_copy[i].front().process_ID << "\"";
				std::cout << " - Next Opr. Time: " << IO_queue_copy[i].front().nextOperationTime;

				std::cout << std::endl;

				found = false;
				v_IO_operations = IO_queue_copy[i].front().processOperationList;
				for (int j = 0; j < v_IO_operations.size(); j++){

					std::cout << "\t";
					if (v_IO_operations[j].operationType == v_IO_operations[j].cpu){
						std::cout << "CPU - Pg: " << v_IO_operations[j].pageNum << "\t";
					}
					else {
						std::cout << "IO" << v_IO_operations[j].ioType << " - Pg: NULL\t";
					}
					std::cout << " - " << v_IO_operations[j].duration;
					std::cout << " - ";

					if (v_IO_operations[j].status == Operation::Status::wait){
						std::cout << "Wait";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].front().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::proc){
						std::cout << "Proc";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].front().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::intr){
						std::cout << "Intr";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].front().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::done){
						std::cout << "Done";
					}

					std::cout << std::endl;

				}

				std::cout << std::endl;
				IO_queue_copy[i].pop();
			}
		}

		if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ){

			std::vector<std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime>> IO_queue_copy = io_queues_PQ;

			if (IO_queue_copy[i].empty()){
				std::cout << "\t*Empty Queue*" << std::endl << std::endl;
			}

			while (!IO_queue_copy[i].empty()){
				if (IO_queue_copy[i].top().Process_next_Task == CREATE) std::cout << "CREATE";
				if (IO_queue_copy[i].top().Process_next_Task == CPU) std::cout << "CPU";
				if (IO_queue_copy[i].top().Process_next_Task == IO) std::cout << "IO";
				if (IO_queue_copy[i].top().Process_next_Task == STOP) std::cout << "STOP";
				if (IO_queue_copy[i].top().Process_next_Task == END) std::cout << "END";
				std::cout << " - \"Process " << IO_queue_copy[i].top().process_ID << "\"";
				std::cout << " - Next Opr. Time: " << IO_queue_copy[i].top().nextOperationTime;

				std::cout << std::endl;

				found = false;
				v_IO_operations = IO_queue_copy[i].top().processOperationList;
				for (int j = 0; j < v_IO_operations.size(); j++){

					std::cout << "\t";
					if (v_IO_operations[j].operationType == v_IO_operations[j].cpu){
						std::cout << "CPU - Pg: " << v_IO_operations[j].pageNum << "\t";
					}
					else {
						std::cout << "IO" << v_IO_operations[j].ioType << " - Pg: NULL\t";
					}
					std::cout << " - " << v_IO_operations[j].duration;
					std::cout << " - ";

					if (v_IO_operations[j].status == Operation::Status::wait){
						std::cout << "Wait";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].top().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::proc){
						std::cout << "Proc";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].top().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::intr){
						std::cout << "Intr";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].top().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::done){
						std::cout << "Done";
					}

					std::cout << std::endl;

				}

				std::cout << std::endl;
				IO_queue_copy[i].pop();
			}
		}

		if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ){

			std::vector<std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_ApproxTime>> IO_queue_copy = io_queues_ApproxPQ;

			if (IO_queue_copy[i].empty()){
				std::cout << "\t*Empty Queue*" << std::endl << std::endl;
			}

			while (!IO_queue_copy[i].empty()){
				if (IO_queue_copy[i].top().Process_next_Task == CREATE) std::cout << "CREATE";
				if (IO_queue_copy[i].top().Process_next_Task == CPU) std::cout << "CPU";
				if (IO_queue_copy[i].top().Process_next_Task == IO) std::cout << "IO";
				if (IO_queue_copy[i].top().Process_next_Task == STOP) std::cout << "STOP";
				if (IO_queue_copy[i].top().Process_next_Task == END) std::cout << "END";
				std::cout << " - \"Process " << IO_queue_copy[i].top().process_ID << "\"";
				std::cout << " - Next Opr. Time: " << IO_queue_copy[i].top().nextOperationTime;

				std::cout << std::endl;

				found = false;
				v_IO_operations = IO_queue_copy[i].top().processOperationList;
				for (int j = 0; j < v_IO_operations.size(); j++){

					std::cout << "\t";
					if (v_IO_operations[j].operationType == v_IO_operations[j].cpu){
						std::cout << "CPU - Pg: " << v_IO_operations[j].pageNum << "\t";
					}
					else {
						std::cout << "IO" << v_IO_operations[j].ioType << " - Pg: NULL\t";
					}
					std::cout << " - " << v_IO_operations[j].duration;
					std::cout << " - ";

					if (v_IO_operations[j].status == Operation::Status::wait){
						std::cout << "Wait";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].top().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::proc){
						std::cout << "Proc";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].top().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::intr){
						std::cout << "Intr";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].top().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::done){
						std::cout << "Done";
					}

					std::cout << std::endl;

				}

				std::cout << std::endl;
				IO_queue_copy[i].pop();
			}
		}

		if (schedulingAlgorithmType == SchedulingAlgorithmType::RR){

			std::vector<std::queue<Process>> IO_queue_copy = io_queues_RR;

			if (IO_queue_copy[i].empty()){
				std::cout << "\t*Empty Queue*" << std::endl << std::endl;
			}

			while (!IO_queue_copy[i].empty()){
				if (IO_queue_copy[i].front().Process_next_Task == CREATE) std::cout << "CREATE";
				if (IO_queue_copy[i].front().Process_next_Task == CPU) std::cout << "CPU";
				if (IO_queue_copy[i].front().Process_next_Task == IO) std::cout << "IO";
				if (IO_queue_copy[i].front().Process_next_Task == STOP) std::cout << "STOP";
				if (IO_queue_copy[i].front().Process_next_Task == END) std::cout << "END";
				std::cout << " - \"Process " << IO_queue_copy[i].front().process_ID << "\"";
				std::cout << " - Next Opr. Time: " << IO_queue_copy[i].front().nextOperationTime;

				std::cout << std::endl;

				found = false;
				v_IO_operations = IO_queue_copy[i].front().processOperationList;
				for (int j = 0; j < v_IO_operations.size(); j++){

					std::cout << "\t";
					if (v_IO_operations[j].operationType == v_IO_operations[j].cpu){
						std::cout << "CPU - Pg: " << v_IO_operations[j].pageNum << "\t";
					}
					else {
						std::cout << "IO" << v_IO_operations[j].ioType << " - Pg: NULL\t";
					}
					std::cout << " - " << v_IO_operations[j].duration;
					std::cout << " - ";

					if (v_IO_operations[j].status == Operation::Status::wait){
						std::cout << "Wait";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].front().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::proc){
						std::cout << "Proc";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].front().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::intr){
						std::cout << "Intr";
						if (found == false){
							found = true;
							if (schedulingAlgorithmType != SchedulingAlgorithmType::ApproxPQ) std::cout << " <--";
							else std::cout << " <-- Approx Duration: " << IO_queue_copy[j].front().approxDuration;
						}
					}

					if (v_IO_operations[j].status == Operation::Status::done){
						std::cout << "Done";
					}

					std::cout << std::endl;

				}

				std::cout << std::endl;
				IO_queue_copy[i].pop();
			}
		}

		// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	}
}

int Virtual_Machine::start(){

	if (loadSuccessful1 == true && loadSuccessful2 == true){

		double smallest;
		int smallestID;
		while (!process_queue.empty()){ // simulation loop

			smallest = controller.max_runtime + 1000000.0;
			smallestID = 0;
			interupted = false;

			if (schedulingAlgorithmType == SchedulingAlgorithmType::RR){

				for (int i = 0; i < totalCpuCount; i++){
					if (RR_Inter_Durations[i] < smallest && CPUs[i] == false){
						smallest = RR_Inter_Durations[i];
						smallestID = i;
					}
				}

				if (smallest < process_queue.top().nextOperationTime && smallest != 0.0){
					if (interupted == false){
						interupted = true;
						inter_ResourceID = smallestID;
						machinesTime = RR_Inter_Durations[smallestID];
					}
				}
			}

			if (interupted == false) machinesTime = process_queue.top().nextOperationTime;


			if (controller.max_runtime != 0.0 && machinesTime > controller.max_runtime){

				machinesTime = controller.max_runtime;

				statistics.makeAverageLatency();
				statistics.makeAverageResponseTime();
				statistics.storeThroughput(numOfTasks);
				statistics.storeEfficiency(utilization, machinesTime, totalCpuCount);

				std::cout << std::fixed;
				std::cout << "-------------------------------------------------" << std::endl;
				std::cout << "Scheduling Algorithm Type: ";
				if (schedulingAlgorithmType == SchedulingAlgorithmType::ERROR) std::cout << "ERROR" << std::endl;
				if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) std::cout << "FIFO" << std::endl;
				if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) std::cout << "PQ" << std::endl;
				if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) std::cout << "ApproxPQ" << std::endl;
				if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) std::cout << "RR" << std::endl;

				// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

				std::cout << "Memory Algorithm Type: ";
				if (memoryAlgorithmType == MemoryAlgorithmType::MEM_ERROR) std::cout << "ERROR" << std::endl;
				if (memoryAlgorithmType == MemoryAlgorithmType::MEM_FIFO) std::cout << "FIFO" << std::endl;
				if (memoryAlgorithmType == MemoryAlgorithmType::MEM_MOST) std::cout << "MOST" << std::endl;
				if (memoryAlgorithmType == MemoryAlgorithmType::MEM_LEAST) std::cout << "LEAST" << std::endl;
				if (memoryAlgorithmType == MemoryAlgorithmType::MEM_SC) std::cout << "SC" << std::endl;

				// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

				std::cout << "Machine's End Time: " << machinesTime << std::endl;
				std::cout << "Macines's Effective CPU End Time: " << utilization << std::endl << std::endl;
				std::cout << "Cache Size: " << cacheMemorySize << std::endl;
				std::cout << "Number of Pages: " << numOfPages << std::endl;
				std::cout << "Num of CPUs: " << totalCpuCount << std::endl;
				std::cout << "Num of IO devices: " << num_of_IO_Devices << std::endl;
				std::cout << "Num of Processes: " << numOfTasks << std::endl;
				statistics.printStats();

				return 1;  // early termination
			}


			// ----- SYSTEM PRINT AND PAUSE -----//
			if (controller.systemPrint == true){
				if (controller.systemPrint_SystemInfo == true) print_System_Info();
				if (controller.systemPrint_CacheInfo == true) print_Cache_vec();
				if (controller.systemPrint_Process_Queue == true) print_Process_Queue();
				if (controller.systemPrint_CPU_Queue == true) print_CPU_Queue();
				if (controller.systemPrint_IO_Queue == true) print_IO_Queues();
			}
			if (controller.systemPause == true){
				system("pause");
			}
			if (controller.systemClear == true){
				system("cls");
			}
			// --- END SYSTEM PRINT AND PAUSE ---//


			//POPING OFF THE PROCESS QUEUE
			handle_Proccess_Queue();
			//END POPING OFF THE PROCESS QUEUE

			// POPING OFF THE CPU QUEUE
			handle_CPU_Queue();
			// END POPING OFF THE CPU QUEUE

			// POPING OFF THE IO QUEUES
			handle_IO_Queue();
			// END POPING OFF THE IO QUEUES


		} // end simulation loop


		// ----- SYSTEM PRINT AND PAUSE -----//
		if (controller.systemPrint == true){
			if (controller.systemPrint_SystemInfo == true) print_System_Info();
			if (controller.systemPrint_CacheInfo == true) print_Cache_vec();
			if (controller.systemPrint_Process_Queue == true) print_Process_Queue();
			if (controller.systemPrint_CPU_Queue == true) print_CPU_Queue();
			if (controller.systemPrint_IO_Queue == true) print_IO_Queues();
		}
		if (controller.systemPause == true){
			system("pause");
		}
		if (controller.systemClear == true){
			system("cls");
		}
		// --- END SYSTEM PRINT AND PAUSE ---//


		statistics.makeAverageLatency();
		statistics.makeAverageResponseTime();
		statistics.storeThroughput(numOfTasks);
		statistics.storeEfficiency(utilization, machinesTime, totalCpuCount);

		std::cout << std::fixed;
		std::cout << "-------------------------------------------------" << std::endl;
		std::cout << "Scheduling Algorithm Type: ";
		if (schedulingAlgorithmType == SchedulingAlgorithmType::ERROR) std::cout << "ERROR" << std::endl;
		if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) std::cout << "FIFO" << std::endl;
		if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) std::cout << "PQ" << std::endl;
		if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) std::cout << "ApproxPQ" << std::endl;
		if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) std::cout << "RR" << std::endl;

		// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

		std::cout << "Memory Algorithm Type: ";
		if (memoryAlgorithmType == MemoryAlgorithmType::MEM_ERROR) std::cout << "ERROR" << std::endl;
		if (memoryAlgorithmType == MemoryAlgorithmType::MEM_FIFO) std::cout << "FIFO" << std::endl;
		if (memoryAlgorithmType == MemoryAlgorithmType::MEM_MOST) std::cout << "MOST" << std::endl;
		if (memoryAlgorithmType == MemoryAlgorithmType::MEM_LEAST) std::cout << "LEAST" << std::endl;
		if (memoryAlgorithmType == MemoryAlgorithmType::MEM_SC) std::cout << "SC" << std::endl;

		// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

		std::cout << "Machine's End Time: " << machinesTime << std::endl;
		std::cout << "Macines's Effective CPU End Time: " << utilization << std::endl << std::endl;
		std::cout << "Cache Size: " << cacheMemorySize << std::endl;
		std::cout << "Number of Pages: " << numOfPages << std::endl;
		std::cout << "Num of CPUs: " << totalCpuCount << std::endl;
		std::cout << "Num of IO devices: " << num_of_IO_Devices << std::endl;
		std::cout << "Num of Processes: " << numOfTasks << std::endl;
		statistics.printStats();

		return 0;  // simulator finished successfully
	}

	return 2; // simulator failed to start

}

void Virtual_Machine::handle_Proccess_Queue(){
	//POPING OFF THE PROCESS QUEUE

	Process process;
	if (interupted == true) {

		std::priority_queue<Process, std::vector<Process>, Process::compareProcesses_OpTime> tempProcessQueue;
		int size = process_queue.size();
		for (int i = 0; i < size; i++){

			process = process_queue.top();
			process_queue.pop();

			if (process.resourceID == inter_ResourceID) break;

			tempProcessQueue.push(process);

		}

		process.last_CPU_Time = machinesTime;
		utilization += controller.RR_Inter_Dur;
		process.processOperationList[process.operationNumber].status = Operation::Status::intr;

		process.processOperationList[process.operationNumber].duration -= controller.RR_Inter_Dur;

		available_CPUs += 1;
		CPUs[process.resourceID] = true;
		process.resourceID = -1;

		cpu_queue_RR.push(process);

		size = tempProcessQueue.size();
		for (int i = 0; i < size; i++){
			process_queue.push(tempProcessQueue.top());
			tempProcessQueue.pop();
		}

	}
	else{
		process = process_queue.top();
		process_queue.pop();

		switch (process.Process_next_Task){

		case CREATE:
			if (process.processOperationList[process.operationNumber].operationType == Operation::Type::cpu){
				process.Process_next_Task = CPU;
				process.processOperationList[process.operationNumber].status = Operation::Status::wait;

				process.setApproxDurration();

				if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) cpu_queue_FIFO.push(process);
				if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) cpu_queue_PQ.push(process);
				if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) cpu_queue_ApproxPQ.push(process);
				if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) cpu_queue_RR.push(process);

				// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

			}
			if (process.processOperationList[process.operationNumber].operationType == Operation::Type::io){
				process.Process_next_Task = IO;
				process.processOperationList[process.operationNumber].status = Operation::Status::wait;

				process.setApproxDurration();

				if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) io_queues_FIFO[process.processOperationList[process.operationNumber].ioType].push(process);
				if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) io_queues_PQ[process.processOperationList[process.operationNumber].ioType].push(process);
				if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) io_queues_ApproxPQ[process.processOperationList[process.operationNumber].ioType].push(process);
				if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) io_queues_RR[process.processOperationList[process.operationNumber].ioType].push(process);

				// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

				IO_Device_Busy[process.processOperationList[process.operationNumber].ioType] = false;
			}
			break;

		case CPU:

			process.last_CPU_Time = machinesTime;
			utilization += process.processOperationList[process.operationNumber].duration;

			available_CPUs += 1;
			if (schedulingAlgorithmType == SchedulingAlgorithmType::RR){
				CPUs[process.resourceID] = true;
				process.resourceID = -1;
			}

			process.processOperationList[process.operationNumber].status = Operation::Status::done;
			process.operationNumber += 1;

			if (process.operationNumber < process.numberOfOperations){
				if (process.processOperationList[process.operationNumber].operationType == Operation::Type::cpu){
					process.Process_next_Task = CPU;
					process.processOperationList[process.operationNumber].status = Operation::Status::wait;

					process.setApproxDurration();

					if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) cpu_queue_FIFO.push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) cpu_queue_PQ.push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) cpu_queue_ApproxPQ.push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) cpu_queue_RR.push(process);

					// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

				}
				if (process.processOperationList[process.operationNumber].operationType == Operation::Type::io){
					process.Process_next_Task = IO;
					process.processOperationList[process.operationNumber].status = Operation::Status::wait;

					process.setApproxDurration();

					if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) io_queues_FIFO[process.processOperationList[process.operationNumber].ioType].push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) io_queues_PQ[process.processOperationList[process.operationNumber].ioType].push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) io_queues_ApproxPQ[process.processOperationList[process.operationNumber].ioType].push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) io_queues_RR[process.processOperationList[process.operationNumber].ioType].push(process);

					// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

				}
			}
			else{
				process.Process_next_Task = END;
			}

			break;

		case IO:

			if (process.first_IO_Done == false){
				process.first_IO_Time = machinesTime;
				process.first_IO_Done = true;
				statistics.storeResponseTime(process);
			}

			IO_Device_Busy[process.processOperationList[process.operationNumber].ioType] = false;
			process.processOperationList[process.operationNumber].status = Operation::Status::done;
			process.operationNumber += 1;

			if (process.operationNumber < process.numberOfOperations){
				if (process.processOperationList[process.operationNumber].operationType == Operation::Type::cpu){
					process.Process_next_Task = CPU;
					process.processOperationList[process.operationNumber].status = Operation::Status::wait;

					process.setApproxDurration();

					if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) cpu_queue_FIFO.push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) cpu_queue_PQ.push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) cpu_queue_ApproxPQ.push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) cpu_queue_RR.push(process);

					// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

				}
				if (process.processOperationList[process.operationNumber].operationType == Operation::Type::io){
					process.Process_next_Task = IO;
					process.processOperationList[process.operationNumber].status = Operation::Status::wait;

					process.setApproxDurration();

					if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO) io_queues_FIFO[process.processOperationList[process.operationNumber].ioType].push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ) io_queues_PQ[process.processOperationList[process.operationNumber].ioType].push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ) io_queues_ApproxPQ[process.processOperationList[process.operationNumber].ioType].push(process);
					if (schedulingAlgorithmType == SchedulingAlgorithmType::RR) io_queues_RR[process.processOperationList[process.operationNumber].ioType].push(process);

					// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

				}
			}
			else{
				process.Process_next_Task = END;
			}
			break;

		case STOP:
			break;

		case END:
			break;

		}
	}

	if (process.Process_next_Task == END){
		statistics.storeLatency(process);
	}

	// ENDING POPING OFF THE PROCESS QUEUE
}


void Virtual_Machine::handle_CPU_Queue(){
	// POPING OFF THE CPU QUEUE

	if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO){
		if (cpu_queue_FIFO.size() > 0 && available_CPUs > 0){

			Process process = cpu_queue_FIFO.front();
			cpu_queue_FIFO.pop();
			available_CPUs -= 1;

			process.processOperationList[process.operationNumber].status = Operation::Status::proc;

			bool applyLoadCost;
			applyLoadCost = handle_Cache_Vec(process.processOperationList[process.operationNumber].pageNum);

			process.nextOperationTime = machinesTime;
			if (applyLoadCost == true) process.nextOperationTime += (process.processOperationList[process.operationNumber].duration + controller.loadCost + controller.contextSwitchCost);
			if (applyLoadCost == false) process.nextOperationTime += (process.processOperationList[process.operationNumber].duration + controller.contextSwitchCost);
			process_queue.push(process);

		}
	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ){
		if (cpu_queue_PQ.size() > 0 && available_CPUs > 0){

			Process process = cpu_queue_PQ.top();
			cpu_queue_PQ.pop();
			available_CPUs -= 1;

			process.processOperationList[process.operationNumber].status = Operation::Status::proc;

			bool applyLoadCost;
			applyLoadCost = handle_Cache_Vec(process.processOperationList[process.operationNumber].pageNum);

			process.nextOperationTime = machinesTime;
			if (applyLoadCost == true) process.nextOperationTime += (process.processOperationList[process.operationNumber].duration + controller.loadCost + controller.contextSwitchCost);
			if (applyLoadCost == false) process.nextOperationTime += (process.processOperationList[process.operationNumber].duration + controller.contextSwitchCost);
			process_queue.push(process);

		}
	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ){
		if (cpu_queue_ApproxPQ.size() > 0 && available_CPUs > 0){

			Process process = cpu_queue_ApproxPQ.top();
			cpu_queue_ApproxPQ.pop();
			available_CPUs -= 1;

			process.setApproxDurration();

			process.processOperationList[process.operationNumber].status = Operation::Status::proc;

			bool applyLoadCost;
			applyLoadCost = handle_Cache_Vec(process.processOperationList[process.operationNumber].pageNum);

			process.nextOperationTime = machinesTime;
			if (applyLoadCost == true) process.nextOperationTime += (process.processOperationList[process.operationNumber].duration + controller.loadCost + controller.contextSwitchCost);
			if (applyLoadCost == false) process.nextOperationTime += (process.processOperationList[process.operationNumber].duration + controller.contextSwitchCost);
			process_queue.push(process);

		}
	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::RR){
		if (cpu_queue_RR.size() > 0 && available_CPUs > 0){

			Process process = cpu_queue_RR.front();
			cpu_queue_RR.pop();
			available_CPUs -= 1;

			process.processOperationList[process.operationNumber].status = Operation::Status::proc;

			for (int i = 0; i < totalCpuCount; i++){
				if (CPUs[i] == true){
					CPUs[i] = false;
					process.resourceID = i;
					RR_Inter_Durations[i] = machinesTime + controller.RR_Inter_Dur + controller.contextSwitchCost;
					break;
				}
			}

			bool applyLoadCost;
			applyLoadCost = handle_Cache_Vec(process.processOperationList[process.operationNumber].pageNum);

			process.nextOperationTime = machinesTime;
			if (applyLoadCost == true) process.nextOperationTime += (process.processOperationList[process.operationNumber].duration + controller.loadCost + controller.contextSwitchCost);
			if (applyLoadCost == false) process.nextOperationTime += (process.processOperationList[process.operationNumber].duration + controller.contextSwitchCost);
			process_queue.push(process);

		}
	}

	// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	// END POPING OFF THE CPU QUEUE
}


bool Virtual_Machine::handle_Cache_Vec(int inputPage){

	bool done = false;
	for (int i = 0; i < cacheMemory.size(); i++){
		if (cacheMemory[i].pageNum == inputPage){
			done = true;
		}
	}

	if (done == true){
		for (int i = 0; i < cacheMemory.size(); i++){
			if (memoryAlgorithmType == MEM_MOST && cacheMemory[i].pageNum == inputPage) cacheMemory[i].status = 1.0;
			else if (memoryAlgorithmType == MEM_MOST && cacheMemory[i].pageNum != inputPage) cacheMemory[i].status = 0.0;
			if (memoryAlgorithmType == MEM_LEAST && cacheMemory[i].pageNum == inputPage) cacheMemory[i].status = machinesTime;
			if (memoryAlgorithmType == MEM_SC && cacheMemory[i].pageNum == inputPage) cacheMemory[i].secondChanceNum += 1.0;
		}
		return false;
	}

	if (memoryAlgorithmType == MEM_FIFO){

		for (int i = 0; i < cacheMemory.size(); i++){
			if (cacheMemory[i].pageNum == -1 && done == false){
				cacheMemory[i].pageNum = inputPage;
				cacheMemory[i].status = 0.0;
				if (i == cacheMemory.size() - 1) cacheMemory[0].status = 1.0;
				else cacheMemory[i + 1].status = 1.0;
				done = true;
			}
		}
		if (done == false) {
			for (int i = 0; i < cacheMemory.size(); i++){
				if (cacheMemory[i].status == 1.0 && done == false){
					cacheMemory[i].pageNum = inputPage;
					cacheMemory[i].status = 0.0;
					if (i == cacheMemory.size() - 1) cacheMemory[0].status = 1.0;
					else cacheMemory[i + 1].status = 1.0;
					done = true;
				}
			}
		}

	}


	if (memoryAlgorithmType == MEM_MOST){

		int actionSpot = -1;

		for (int i = 0; i < cacheMemory.size(); i++){
			if (cacheMemory[i].pageNum == -1 && done == false){
				cacheMemory[i].pageNum = inputPage;
				cacheMemory[i].status = 1.0;
				actionSpot = i;
				done = true;
			}
		}
		if (done == false){
			for (int i = 0; i < cacheMemory.size(); i++){
				if (cacheMemory[i].status == 1.0 && done == false){
					cacheMemory[i].pageNum = inputPage;
					cacheMemory[i].status = 1.0;
					actionSpot = i;
					done = true;
				}
			}
		}
		for (int i = 0; i < cacheMemory.size(); i++){
			if (i != actionSpot){
				cacheMemory[i].status = 0.0;
			}
		}

	}


	if (memoryAlgorithmType == MEM_LEAST){

		double smallest = controller.max_runtime + 1000000.0;

		for (int i = 0; i < cacheMemory.size(); i++){
			if (cacheMemory[i].pageNum == -1 && done == false){
				cacheMemory[i].pageNum = inputPage;
				cacheMemory[i].status = machinesTime;
				done = true;
			}
		}
		if (done == false){
			for (int i = 0; i < cacheMemory.size(); i++){
				if (cacheMemory[i].status < smallest) smallest = cacheMemory[i].status;
			}

			for (int i = 0; i < cacheMemory.size(); i++){
				if (cacheMemory[i].status == smallest && done == false){
					cacheMemory[i].pageNum = inputPage;
					cacheMemory[i].status = machinesTime;
					done = true;
				}
			}
		}

	}


	if (memoryAlgorithmType == MEM_SC){

		for (int i = 0; i < cacheMemory.size(); i++){
			if (cacheMemory[i].pageNum == -1 && done == false){
				cacheMemory[i].pageNum = inputPage;
				cacheMemory[i].status = 0.0;
				if (i == cacheMemory.size() - 1) cacheMemory[0].status = 1.0;
				else cacheMemory[i + 1].status = 1.0;
				done = true;
			}
		}
		if (done == false) {
			handle_Cache_Vec_SC(inputPage, done);
		}

	}

	// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	return true;
}

void Virtual_Machine::handle_Cache_Vec_SC(int inputPage, bool done){
	for (int i = 0; i < cacheMemory.size(); i++){
		if (cacheMemory[i].status == 1.0 && done == false){
			if (cacheMemory[i].secondChanceNum > 0.0){
				cacheMemory[i].secondChanceNum -= 1.0;
				cacheMemory[i].status = 0.0;
				if (i == cacheMemory.size() - 1) cacheMemory[0].status = 1.0;
				else cacheMemory[i + 1].status = 1.0;
			}
			else{
				cacheMemory[i].pageNum = inputPage;
				cacheMemory[i].status = 0.0;
				if (i == cacheMemory.size() - 1) cacheMemory[0].status = 1.0;
				else cacheMemory[i + 1].status = 1.0;
				done = true;
			}
		}
	}
	if (done == false) handle_Cache_Vec_SC(inputPage, done);
}


void Virtual_Machine::handle_IO_Queue(){
	// POPING OFF THE IO QUEUES

	if (schedulingAlgorithmType == SchedulingAlgorithmType::FIFO){
		for (int i = 0; i < num_of_IO_Devices; i++){
			if (io_queues_FIFO[i].size() > 0 && IO_Device_Busy[i] == false){

				Process process;
				process = io_queues_FIFO[i].front();
				io_queues_FIFO[i].pop();
				IO_Device_Busy[i] = true;

				process.processOperationList[process.operationNumber].status = Operation::Status::proc;

				process.nextOperationTime = machinesTime;
				process.nextOperationTime += process.processOperationList[process.operationNumber].duration;
				process_queue.push(process);

			}
		}
	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::PQ){
		for (int i = 0; i < num_of_IO_Devices; i++){
			if (io_queues_PQ[i].size() > 0 && IO_Device_Busy[i] == false){

				Process process;
				process = io_queues_PQ[i].top();
				io_queues_PQ[i].pop();
				IO_Device_Busy[i] = true;

				process.processOperationList[process.operationNumber].status = Operation::Status::proc;

				process.nextOperationTime = machinesTime;
				process.nextOperationTime += process.processOperationList[process.operationNumber].duration;
				process_queue.push(process);

			}
		}
	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::ApproxPQ){
		for (int i = 0; i < num_of_IO_Devices; i++){
			if (io_queues_ApproxPQ[i].size() > 0 && IO_Device_Busy[i] == false){

				Process process;
				process = io_queues_ApproxPQ[i].top();
				io_queues_ApproxPQ[i].pop();
				IO_Device_Busy[i] = true;

				process.setApproxDurration();

				process.processOperationList[process.operationNumber].status = Operation::Status::proc;

				process.nextOperationTime = machinesTime;
				process.nextOperationTime += process.processOperationList[process.operationNumber].duration;
				process_queue.push(process);

			}
		}
	}

	if (schedulingAlgorithmType == SchedulingAlgorithmType::RR){
		for (int i = 0; i < num_of_IO_Devices; i++){
			if (io_queues_RR[i].size() > 0 && IO_Device_Busy[i] == false){

				Process process;
				process = io_queues_RR[i].front();
				io_queues_RR[i].pop();
				IO_Device_Busy[i] = true;

				process.processOperationList[process.operationNumber].status = Operation::Status::proc;

				process.nextOperationTime = machinesTime;
				process.nextOperationTime += process.processOperationList[process.operationNumber].duration;
				process_queue.push(process);

			}
		}
	}

	// ---------------------------------------- ADDITIONAL INSTALLATIONS GO HERE ---------------------------------------- //

	// END POPING OFF THE IO QUEUES
}






double Virtual_Machine::getMachinesTime(){
	return machinesTime;
}

double Virtual_Machine::getUtilization(){
	return utilization;
}

template< class T >
T Virtual_Machine::getRandNum(T lowerBound, T upperBound){
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_real_distribution<> urd(lowerBound, upperBound + 1);
	return urd(mt);
}
