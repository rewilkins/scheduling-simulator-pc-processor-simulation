#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <iostream>
#include <random>

template < class T >
T getRandNum(T lowerBound, T upperBound){
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_real_distribution<> urd(lowerBound, upperBound + 1);
	return urd(mt);
}

class Controller {
public:


	// Allow System to print at each event
	bool systemPrint = false;  // print master switch

	bool systemPrint_SystemInfo = true;
	bool systemPrint_CacheInfo = true;
	bool systemPrint_Process_Queue = true;
	bool systemPrint_CPU_Queue = true;
	bool systemPrint_IO_Queue = true;


	// Allow System to pause at each event.  Does not work on Linux systems.
	bool systemPause = true;


	// Allow System to clear the screen at each event.  Does not work on Linux systems.
	bool systemClear = false;


	// Adjust the max time the simulator will run for (Range is 0.0 to ... ):
	// 0.0 = infinite runtime (untill all processes are finished)
	double max_runtime = 0.0;


	// Adjust the duration between Round Robin interuptions (Range is 0.0 to ... ):
	double RR_Inter_Dur = 2.0;


	// Adjust the VM's Cache/Memory size (Range is 1 to ... ):
	int cacheMemorySize_MIN_LIMIT = 15;
	int cacheMemorySize_MAX_LIMIT = 15;


	// Adjust the allowed page number range (Range is 1 to ... ):
	int pageDistribution_MIN_LIMIT = 1;
	int pageDistribution_MAX_LIMIT = 10;


	// Adjust the number of CPUs the simulator has (Range is 1 to ... ):
	int CPUs_MIN_LIMIT = 2;
	int CPUs_MAX_LIMIT = 2;


	// Adjust the number of unique IO devices the simulator excepts (Range is 1 to ... ):
	int IO_DEVICE_MIN_LIMIT = 2;
	int IO_DEVICE_MAX_LIMIT = 2;


	// Adjust the number of processes the simulator has (Range is 1 to ... ):
	int PROCESS_MIN_COUNT = 1000;
	int PROCESS_MAX_COUNT = 1000;


	// Adjust the number of operations a process can perform (Range is 1 to ... ):
	int PROCESS_OPERATIONS_MIN_COUNT = 15;
	int PROCESS_OPERATIONS_MAX_COUNT = 15;


	// Adjust the difference between the CPU and I/O time durations for process operations (Range is 0 to 100):
	// 0 to 49 = a CPU heavy Process, CPU operations have long durations and IO operations have short durations
	// 50 = evenly distributed operation durations between limits
	// 51 to 100 = an IO heavy Process, CPU operations have short durations and IO operations have long durations
	int taskMix = 50;
	double taskMix_min_limit = 1.0;
	double taskMix_max_limit = 10.0;


	// Adjust the frequency at wich tasks are created; the range of starting times a process can have (Range is 0 to 100):
	// 0 = min limit
	// Lower # = all tasks scheduled with similar start times and close to the min limit.
	// Higher # = all tasks evenly distributed between limits
	// 100 = all tasks evenly distributed between limits
	double taskFrequency = 100;
	double taskFrequency_min_limit = 1.0;
	double taskFrequency_max_limit = 10.0;


	// Adjust the simulated cost of context switching (Range is 0.0 to ... ):
	double contextSwitchCost = 3.0;


	// Adjust the simulated cost of loading data from storage to cache (Range is 0.0 to ... ):
	double loadCost = 5.0;
	// loadCost + contextSwitchCost must be < the RR quanta

	//-------------------------------------------------------------------------------------------------------------------------------------------//


	Controller(){

		// controller safty checks and overrides
		if (systemPrint == false) systemPause = false;
		if (systemPrint == false) systemClear = false;
		if (max_runtime < 0) max_runtime = 0.0;
		if (RR_Inter_Dur < 0.0) RR_Inter_Dur = 0.0;
		if (cacheMemorySize_MIN_LIMIT < 0.0) cacheMemorySize_MIN_LIMIT = 0.0;
		if (pageDistribution_MIN_LIMIT < 0.0) pageDistribution_MIN_LIMIT = 0.0;
		if (CPUs_MIN_LIMIT < 1) CPUs_MIN_LIMIT = 1;
		if (CPUs_MIN_LIMIT > CPUs_MAX_LIMIT) CPUs_MIN_LIMIT = CPUs_MAX_LIMIT;
		if (IO_DEVICE_MIN_LIMIT < 1) IO_DEVICE_MIN_LIMIT = 1;
		if (IO_DEVICE_MIN_LIMIT > IO_DEVICE_MAX_LIMIT) IO_DEVICE_MIN_LIMIT = IO_DEVICE_MAX_LIMIT;
		if (taskMix < 0) taskMix = 0;
		if (taskMix > 100) taskMix = 100;
		if (taskMix_min_limit <= 0.0) taskMix_min_limit = 1.0;
		if (taskMix_min_limit > taskMix_max_limit) taskMix_min_limit = taskMix_max_limit;
		if (taskFrequency < 0) taskFrequency = 0;
		if (taskFrequency > 100) taskFrequency = 100;
		if (taskFrequency_min_limit <= 0.0) taskFrequency_min_limit = 1.0;
		if (taskFrequency_min_limit > taskFrequency_max_limit) taskFrequency_min_limit = taskFrequency_max_limit;
		if (PROCESS_OPERATIONS_MIN_COUNT < 1) PROCESS_OPERATIONS_MIN_COUNT = 1;
		if (PROCESS_OPERATIONS_MIN_COUNT > PROCESS_OPERATIONS_MAX_COUNT) PROCESS_OPERATIONS_MIN_COUNT = PROCESS_OPERATIONS_MAX_COUNT;
		if (contextSwitchCost < 0.0) contextSwitchCost = 0.0;
		if (loadCost < 0.0) loadCost = 0.0;
		if (PROCESS_MIN_COUNT < 1) PROCESS_MIN_COUNT = 1;
		if (PROCESS_MIN_COUNT > PROCESS_MAX_COUNT) PROCESS_MIN_COUNT = PROCESS_MAX_COUNT;
		// end safty checks

	}

};

#endif



