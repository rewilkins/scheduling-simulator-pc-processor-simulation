Open up the file called Controller.hpp.  This is how you change 
variables to control the simulator.

Once you run the program, the main funciton has a console UI before 
the VM starts.  The UI will ask for two imputs one at a time.  From the 
first provided list, type in the name of the scheduling algorithm you 
want to run then hit enter.  The input will be logged and the UI will ask
for the second input.  From the second provided list, type in the name 
of the memory algorithm you want to run then hit enter.  The VM will
then start with the  selected algorithms.

The VM will run and exit in 1 of 3 states:
state 0 = success
state 1 = early termination
state 2 = failed to start

you will get a "failed to start" error if you incorrectly type
a valid algorithm name from the provided lists.  The names
are not case sensitive.
